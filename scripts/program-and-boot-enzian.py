#!/usr/bin/env python3

import time
from time import sleep
import paramiko
import argparse
import pexpect
import os
import re
import binascii

"""
This automation script is used to load up the enzian fpga with
a bitstream and an optional payload.

This script has to be run from within the ETH network(VPN).

It is assumed that the user has already built the bitstream,
and that the tmux sessions are already set up on enzian-gateway and enzian-build.
For this, you can use the command tmux new -s <session_name> on enzian-gateway and enzian-build to create sessions with the names cpu, fpga, emg and build.

Also, the consoles of emg, cpu and fpga should be attached to the tmux sessions on enzian-gateway.
e.g. emg boot zuestoll<number>, console zuestoll<number>-console, console zuestoll<number>-fpga.
The script will:
- ssh into enzian-gateway and enzian-build
- attach to the tmux sessions
- restart enzian if the option is set
- power on the cpu and fpga
- pause the booting process of the cpu side
- flash the bitstream from enzian-build onto the fpga
- resume the booting process of the cpu side
- optionally load the payload from enzian-gateway
- detach from the tmux sessions and exits


"""

bmc_console = None
fpga_console = None
cpu_console = None

ENZIAN_BUILD_HOST = "enzian-build.ethz.ch"
ENZIAN_GATEWAY_HOST = "enzian-gateway.ethz.ch"
RISCV_PROMPT = "enzian@enzian-riscv:~$ "

parser = argparse.ArgumentParser()

# Workload parameters
parser.add_argument(
    "machine",
    type=str,
    help="The target enzian",
    choices=[f"zuestoll0{x}" for x in list(range(1, 9 + 1))],
)
parser.add_argument("--bitstream", type=str, help="bitstream name")
parser.add_argument(
    "--bitstream-dir",
    type=str,
    help="bitstream directory on enzian-build,"
    " default is /scratch/<user>/bitstreams",
)
parser.add_argument(
    "--payload",
    type=str,
    help="optional payload binary to load into FPGA memory."
    " Will be copied to /scratch/<user> on enzian-gateway."
    " This assumes /scratch/<user> is mounted on the CPU",
)

parser.add_argument("--outdir", type=str, help="output directory when reading files")

parser.add_argument(
    "--targetdir", type=str, help="target directory on the soft core when reading files"
)

parser.add_argument(
    "--norestart", action="store_true", help="disable power_down() call"
)

# User & password
parser.add_argument("--user", type=str, required=True)
parser.add_argument("-pw", "--password", type=str, required=True)

# Parse
args = parser.parse_args()


def ssh_console_attach(username: str, password: str, console: str) -> paramiko.Channel:
    # Establish an SSH session
    client: paramiko.SSHClient = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ENZIAN_GATEWAY_HOST, username=username, password=password)

    # Check that we own the bmc console before connecting
    if "bmc" in console:
        _, stdout, _ = client.exec_command(f"console -w {console}\n", get_pty=True)
        if f"emg-{username}" not in str(stdout.read()):
            raise Exception(f"Console {console} not reserved! Exiting")

    # We use a shell instead of 'exec_command' as stdout is otherwise only readable *after* the command exits
    shell = client.invoke_shell()
    shell.resize_pty(width=160, height=24, width_pixels=900, height_pixels=200)

    shell.send(f"console -f {console}\n".encode())
    # Consume the initial messages
    sleep(1)
    shell.recv(2048)
    return shell


def detach_and_close(channel: paramiko.Channel):
    # Send "^Ec." to detach the console
    channel.send(b"\x05")
    channel.send(b"c.\n")
    channel.close()


def power_down():
    print("powering down...")
    bmc_console.send(b"\n")  # to make sure we are in the python shell
    bmc_console.send(b"power_down()\n")
    wait_for_prompt(bmc_console, b">>> ")


def wait_for_prompt(channel: paramiko.Channel, msg: bytes, clean=False):
    # Wait until we receive a string ending with the given prompt
    out_b = b""
    out_str = ""
    while True:
        while not channel.recv_ready():
            pass
        # Log everything in case we cut off a prompt we are looking for
        recv_b = channel.recv(1024)
        out_b += recv_b
        if clean:
            msg_str = str(msg)
            ansi_regex = re.compile(r"(\x9b|\x1b\[)[0-?]*[ -/]*[@-~]")
            out_str += recv_b.decode()
            out_str = ansi_regex.sub("", out_str)
            if out_str.endswith(msg_str):
                # Filter out ANSI color sequences
                return (out_str, out_b)
        else:
            if out_b.endswith(msg):
                # Return everything except the expected prompt
                return out_b[: -(len(msg))]


def power_up_and_pause_boot():
    print("powering up...")
    bmc_console.send(b"\n")  # to make sure we are in the python shell
    bmc_console.send(b"common_power_up()\n")
    wait_for_prompt(bmc_console, b">>> ")
    bmc_console.send(b"fpga_power_up()\n")
    wait_for_prompt(bmc_console, b">>> ")
    bmc_console.send(b"cpu_power_up()\n")
    wait_for_prompt(bmc_console, b">>> ")
    # Bring up the boot menu on the CPU
    wait_for_prompt(cpu_console, b"Press 'B' for boot menu (10 sec)\r\n")
    cpu_console.send(b"b")
    wait_for_prompt(cpu_console, b"Choice: ")


def write_bitstream_to_fpga(
    username: str,
    password: str,
    bitstream: str,
    machine: str,
):
    client: paramiko.SSHClient = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ENZIAN_BUILD_HOST, username=username, password=password)

    tcl_script_path = (
        f"/scratch/{username}/enzian-chipyard/fpga/scripts/program_bitstream.tcl"
    )
    command_str = f"/opt/Xilinx/Vivado/2020.1/bin/vivado -mode batch -source {tcl_script_path} -tclargs {machine} {bitstream}"
    stdin, stdout, stderr = client.exec_command(command_str)
    # Check that it worked
    if stdout.channel.recv_exit_status() != 0:
        raise ValueError(f"Unable to write bitstream: {stderr.read()}")

    # Make sure the ECI bootloader has been executed *before* we boot the CPU
    wait_for_prompt(fpga_console, b"Press any key to boot\r\n")


def resume_booting():
    cpu_console.send(b"n")
    wait_for_prompt(cpu_console, b"login: ")
    cpu_console.send(b"enzian\n")
    wait_for_prompt(cpu_console, b"Password: ")
    cpu_console.send(b"enzian\n")
    wait_for_prompt(cpu_console, b":~$ ")


def send_riscv_compatible(console : paramiko.Channel, msg : bytes):
    # For some reason, we can only send 8 bytes at a time to the CPU
    total_len = len(msg)
    index = 0
    while index < total_len:
        console.send(msg[index:(index + 8)])
        index += 8
        sleep(0.1)

def copy_dir_recursive(target: str, output: str):
    # Copy a directory and its contents recursively
    # Execute 'ls -l'
    ls_cmd = (f"ls -l {target}\n").encode()
    send_riscv_compatible(fpga_console, ls_cmd)
    result, _ = wait_for_prompt(fpga_console, RISCV_PROMPT, clean=True)
    result = str(result)
    if "No such file or directory" in result:
        raise ValueError(f"ERROR: No such file or directory: {target}")
    # Parse the list of files and directories
    result_lines = result.splitlines()
    for line in result_lines:
        if line.startswith("d"):
            # last entry = name of file/directory
            dir_ = line.split(" ")[-1]
            # Recursively copy this directory
            copy_dir_recursive(f"{target}/{dir_}", f"{output}/{dir_}")
        elif line.startswith("-r"):
            # This must be a file - try to read it
            size = line.split(" ")[4]
            file_ = line.split(" ")[-1]
            read_file(f"{target}/{file_}", int(size), f"{output}/{file_}")

    return


def read_file(target: str, size: int, output: str):
    # Make sure the path exists, and if not, create it
    os.makedirs(os.path.dirname(output), exist_ok=True)
    # Execute 'cat file' and put the output into a file
    print(f"reading file {target} of size {size}")
    cat_cmd = f"cat {target}\n".encode()
    hexdump_cmd = f"hexdump -ve \'2/1 \"%02x\"\' {target} && echo\n".encode()
    send_riscv_compatible(fpga_console, hexdump_cmd)
    # recv the command
    fpga_console.recv(len(hexdump_cmd)+11)
    # recv the file encoded in hex
    file_hex = b""
    for i in range(size):
        # To make sure we read *exactly* 2 bytes (not at most two bytes, once)
        file_hex += fpga_console.recv(1)
        file_hex += fpga_console.recv(1)

    wait_for_prompt(fpga_console, RISCV_PROMPT, clean=True)
    file_b = binascii.unhexlify(file_hex)
    with open(output, "wb") as outfile:
        outfile.write(file_b)


def bring_up_core():
    if not args.norestart:
        power_down()

    power_up_and_pause_boot()

    bitstream_dir = f"/scratch/{args.user}/bitstreams"
    if args.bitstream_dir is not None:
        bitstream_dir = args.bitstream_dir
    bitstream = bitstream_dir + "/" + args.bitstream

    print(f"writing bitstream {bitstream}")
    write_bitstream_to_fpga(args.user, args.password, bitstream, args.machine)

    print("resuming boot...")
    resume_booting()

    if args.payload is not None:
        payload_basename = os.path.basename(args.payload)
        payload_path_remote = (
            f"/scratch/{args.user}/{payload_basename}"  # enzian-gateway
        )
        scp_target = f"{args.user}@{ENZIAN_GATEWAY_HOST}:{payload_path_remote}"

        # TODO: Add option to copy from a remote host
        print(f"copying payload {args.payload} to {scp_target}")
        scp_cmd = f"scp {args.payload} {scp_target}"
        scp = pexpect.spawn(scp_cmd)
        scp.expect("password:")
        scp.sendline(f"{args.password}")
        scp.wait()

        print(f"loading payload {args.payload} onto FPGA ...")
        load_script = f"/scratch/{args.user}/loadFiles"  # enzian-gateway
        command_str = f"sudo {load_script} {payload_path_remote} 0\n"
        cpu_console.send(command_str.encode())

        result = wait_for_prompt(cpu_console, b":~$ ")
        if b"Done" not in result:
            raise Exception(f"Loading payload failed with Error: {str(result)}")

        print("payload loaded")
        print("booting fpga...")
        fpga_console.send(b"\n")


def main() -> None:
    time_start = time.perf_counter()

    global bmc_console
    global cpu_console
    global fpga_console

    try:
        print("creating ssh channels...")
        bmc_console = ssh_console_attach(
            args.user, args.password, f"{args.machine}-bmc"
        )
        print("bmc attached")
        # don't forget, the emg session should have an emptyline first for the python interface to work. because of the 'wrong pid' bug/error.
        cpu_console = ssh_console_attach(
            args.user, args.password, f"{args.machine}-console"
        )
        print("cpu attached")

        fpga_console = ssh_console_attach(
            args.user, args.password, f"{args.machine}-fpga"
        )
        print("fpga attached")

        if args.bitstream is not None:
            bring_up_core()

        if args.targetdir is not None:
            # Sanity check that the core is up, and if not, wait:
            fpga_console.send(b"\n")
            wait_for_prompt(fpga_console, RISCV_PROMPT, clean=True)

            output_dir = f"{os.getcwd()}/{args.targetdir}"
            if args.outdir is not None:
                output_dir = args.targetdir
            print(f"out: {output_dir}")

            copy_dir_recursive(args.targetdir, output_dir)

        print("finished")
        time_end = time.perf_counter()
        print(f"Total execution time: {time_end-time_start:0.4f}s")
    except Exception as e:
        print(e)
    finally:
        print("\nDetaching consoles and closing connections")
        detach_and_close(bmc_console)
        detach_and_close(cpu_console)
        detach_and_close(fpga_console)


if __name__ == "__main__":
    main()
