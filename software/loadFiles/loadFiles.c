#include <fcntl.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>

// CCPI 1
// 0x0000010000000000 // mem
// 0x000001ffffffffff
// 0x0000900000000000 // i/o ncb <=========== We want to write here.
// 0x000097dfffffffff
// 0x000097e000000000 // i/o rsl
// 0x000097e0ffffffff
// 0x000097f000000000 // i/o ap
// 0x000097ffffffffff
// 0x0000980000000000 // i/o sli
// 0x000098ffffffffff

const uint64_t IO_NCB_BASE = 0x0000900000000000ULL;
const uint64_t IO_NCB_END = 0x000097dfffffffffULL;
const uint64_t IO_NCB_SIZE = IO_NCB_END - IO_NCB_BASE;

/**
 * Main function of the program.
 *
 * This function serves as the main function of the program.
 *
 * @param argc The number of arguments the program received.
 * @param argv The arguments of the program.
 * @return This function returns EXIT_FAILURE on failure and EXIT_SUCCESS
 * otherwise.
 */
int main(int argc, char *argv[]) {

  printf("IO_NCB_BASE: 0x%lx\n", IO_NCB_BASE);

  if (argc < 3) {
    printf("USAGE: ./loader pathToFileToLoad offset\n");
    exit(EXIT_FAILURE);
  }

  uint64_t offset = strtol(argv[2], NULL, 16);
  if (offset > IO_NCB_SIZE) {
    printf("offset must be smaller than %lx.\n", IO_NCB_SIZE);
    exit(EXIT_FAILURE);
  }

  int dst_fd = 0;
  int src_fd = 0;

  dst_fd = open("/dev/mem", O_RDWR);
  if (!dst_fd) {
    printf("Failed to open /dev/mem.\n");
    exit(EXIT_FAILURE);
  }

  src_fd = open(argv[1], O_RDWR);
  if (!src_fd) {
    printf("Failed to open %s.\n", argv[1]);
    exit(EXIT_FAILURE);
  }

  struct stat st;
  stat(argv[1], &st);
  size_t file_size = st.st_size;
  if (file_size > (IO_NCB_SIZE - offset)) {
    printf("File is too big.\n");
    exit(EXIT_FAILURE);
  }

  char *vrt_addr = mmap(0, file_size, PROT_WRITE, MAP_SHARED, dst_fd,
                        IO_NCB_BASE + offset);
  if (vrt_addr == MAP_FAILED) {
    printf("Failed to mmap /dev/mem at offset %lx.\n", IO_NCB_BASE + offset);
    exit(EXIT_FAILURE);
  }

  char *file_addr = mmap(0, file_size, PROT_WRITE, MAP_SHARED, src_fd, 0);
  if (file_addr == MAP_FAILED) {
    printf("Failed to mmap /dev/mem at offset %lx.\n", IO_NCB_BASE + offset);
    exit(EXIT_FAILURE);
  }

  printf("Starting to write file, file_size=%lu ...\n", file_size);
  for (int i = 0; i < file_size; i++) {
      vrt_addr[i] = file_addr[i];
  }

  printf("Done\n");

  return 0;
}
