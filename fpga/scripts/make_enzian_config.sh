#!/bin/bash

# Displays avaiable configs, invokes make for it, and renames & moves the resulting report and bitstream

target_dir="/scratch/rstarc"
bitstreams_dir="$target_dir/bitstreams"
reports_dir="$target_dir/reports"

function build_config(){
  config=$1
  bitstream_file="generated-src/chipyard.fpga.enzian.EnzianFPGATestHarness."$config"/obj/EnzianFPGATestHarness.bit"
  report_config_dir="generated-src/chipyard.fpga.enzian.EnzianFPGATestHarness."$config"/obj/report"
  dts_file="generated-src/chipyard.fpga.enzian.EnzianFPGATestHarness."$config"/chipyard.fpga.enzian.EnzianFPGATestHarness."$config".dts"
  mkdir -p "$reports_dir"/"$config"
  make SUB_PROJECT=enzian CONFIG="$config" bitstream 2>&1 | tee -a "$reports_dir/"$config"/build.log"
  cp "$bitstream_file" "$bitstreams_dir"/"$config".bit
  cp -r "$report_config_dir" "$reports_dir"/"$config"
  cp "$dts_file" "$reports_dir/$config/$config.dts"
}

current_dir=${PWD##*/}
if [ "$current_dir" != "fpga" ]; then
  echo "Invoke from 'fpga' directory!"
  exit -1
fi

# Get available configs from Configs.scala
config_file=$(realpath -e "src/main/scala/enzian/Configs.scala")
config_regex="^class\s(Rocket\S*|Boom\S*)Config"
configs_array=($(cat "$config_file" | grep -Eo "$config_regex" | cut -d " " -f 2))

COLUMNS=1
PS3="Select a config"
  select config in ${configs_array[@]}
  do
    build_config $config
    break;
  done
