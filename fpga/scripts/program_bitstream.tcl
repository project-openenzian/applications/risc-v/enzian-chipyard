# This script programs the specified bitstream onto the specified enzian using vivado
# Usage:
#   vivado -mode batch -source program_bistream.tcl -tclargs zuestollXX bitstream.bit

# Parse the arguments
if { $argc != 2 } {
  puts "Usage: $argv0 machine bitstream"
  exit 1
}
# Note: bitstream argument is not checked at any point
set machine [lindex $argv 0]
set bitstream [lindex $argv 1]

# Look up the JTAG
# Not the most elegant solution, but couldn't find an easily parseable single source of truth on this
dict set jtag zuestoll01 210357A7CB93A
dict set jtag zuestoll02 210357A7CB89A
dict set jtag zuestoll03 210357A7CB8DA
dict set jtag zuestoll04 210357A7CB8FA
dict set jtag zuestoll05 210357A7CB8CA
dict set jtag zuestoll06 210357A7CB91A
dict set jtag zuestoll07 210357A7CB88A
dict set jtag zuestoll08 210357A7CB8EA
dict set jtag zuestoll09 210357A7CB8BA

if {[dict exists $jtag $machine]} {
  set target_jtag [dict get $jtag $machine]
  puts "JTAG ID: $target_jtag"
} else {
  puts "machine \"$machine\" not found"
  exit 1
}

# Program the bitstream
open_hw_manager
connect_hw_server -url enzian-gateway.ethz.ch:3121
set hw_target "*/xilinx_tcf/Digilent/${target_jtag}"
puts "hw_target: $hw_target"
current_hw_target [get_hw_targets $hw_target]
open_hw_target
set_property PROGRAM.FILE "$bitstream" [get_hw_devices xcvu9p_0]
set_property PROBES.FILE {} [get_hw_devices xcvu9p_0]
set_property FULL_PROBES.FILE {} [get_hw_devices xcvu9p_0]
current_hw_device [lindex [get_hw_devices] 0]
program_hw_devices [lindex [get_hw_devices] 0]
refresh_hw_device [lindex [get_hw_devices] 0]
quit
