#include <stdint.h>

#include <platform.h>

#define DEBUG
#include "kprintf.h"

extern char _ldpayload;
extern char _payload;
extern char* saved_epayload;

int copy(void) {
    char *src = &_ldpayload;
    char *dst = &_payload;

	REG32(uart, UART_REG_TXCTRL) = UART_TXEN;

    kputs("LOAD");

    while (dst < saved_epayload) {
        *dst++ = *src++;
    }

    src = &_ldpayload;
    dst = &_payload;

    while (dst < saved_epayload) {
        if (*dst++ != *src++) {
            kputs("ERROR");
        }
    }

    kputs("DONE");

	__asm__ __volatile__ ("fence.i" : : : "memory");

    return 0;
}