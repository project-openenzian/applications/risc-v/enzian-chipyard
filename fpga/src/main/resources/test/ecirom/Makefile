# RISCV environment variable must be set
ROOT_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
BUILD_DIR := $(ROOT_DIR)/build
LINKER_DIR := $(ROOT_DIR)/linker

CC=$(RISCV)/bin/riscv64-unknown-elf-gcc
OBJCOPY=$(RISCV)/bin/riscv64-unknown-elf-objcopy
OBJDUMP=$(RISCV)/bin/riscv64-unknown-elf-objdump
HEXDUMP=hexdump -C
CFLAGS=-march=rv64ima -mcmodel=medany -O2 -std=gnu11 -Wall -nostartfiles
CFLAGS+= -fno-common -g -DENTROPY=0 -mabi=lp64 -DNONSMP_HART=0
CFLAGS+= -I $(ROOT_DIR)/include -I.
LFLAGS=-static -nostdlib -L $(LINKER_DIR) -T ecirom.elf.lds

ROM_ORIGIN ?= 0x1000000
ROM_LENGTH ?= 0x4000000
RAM_ORIGIN ?= 0x80000000
RAM_LENGTH ?= 0x4000000

default: elf bin dump

mem := $(LINKER_DIR)/memory.lds
$(mem): $(LINKER_DIR)/memory_template.lds FORCE
	cp $< $@
	sed -i 's/ROM_ORIGIN/$(ROM_ORIGIN)/g' $@
	sed -i 's/ROM_LENGTH/$(ROM_LENGTH)/g' $@
	sed -i 's/RAM_ORIGIN/$(RAM_ORIGIN)/g' $@
	sed -i 's/RAM_LENGTH/$(RAM_LENGTH)/g' $@

.PHONY: mem
mem: $(mem)

elf := $(BUILD_DIR)/ecirom.elf
$(elf): $(mem) head.S test.c kprintf.c FORCE
	mkdir -p $(BUILD_DIR)
	$(CC) $(CFLAGS) $(LFLAGS) -o $@ head.S test.c kprintf.c

.PHONY: elf
elf: $(elf)

bin := $(BUILD_DIR)/ecirom.bin
$(bin): $(elf) FORCE
	mkdir -p $(BUILD_DIR)
	$(OBJCOPY) -O binary --change-addresses=-0x10000 $< $@

.PHONY: bin
bin: $(bin)

dump := $(BUILD_DIR)/ecirom.dump
$(dump): $(elf) FORCE
	$(OBJDUMP) -D -S $< > $@

.PHONY: dump
dump: $(dump)

asm := $(BUILD_DIR)/asm
$(asm): $(elf)
	$(OBJDUMP) --disassemble-all $< > $@

.PHONY: asm
asm: $(asm)

hexdump := $(BUILD_DIR)/hexdump
$(hexdump): $(bin)
	$(HEXDUMP) $< > $@

.PHONY: hexdump
hexdump: $(hexdump)

.PHONY: clean
clean::
	rm -rf $(BUILD_DIR)
	rm $(mem)

.PHONY: FORCE
FORCE: