#include <stdint.h>

#include <platform.h>

#define DEBUG
#include "kprintf.h"

extern char _ram_start;

int test(void) {
    char *ram_start = &_ram_start;
    unsigned long *ram_start_long = (unsigned long*) ram_start;

	REG32(uart, UART_REG_TXCTRL) = UART_TXEN;
	REG32(uart, UART_REG_RXCTRL) = UART_RXEN;
	volatile uint32_t *rx = &REG32(uart, UART_REG_RXFIFO);
    
    kputs("Press any key to boot");
	while ((int32_t)(*rx) < 0);
    kputs("A key was pressed");

    return 0;
}