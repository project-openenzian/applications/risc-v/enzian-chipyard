package chipyard.fpga.enzian

import sys.process._

import freechips.rocketchip.config.{Config, Parameters}
import freechips.rocketchip.subsystem.{SystemBusKey, PeripheryBusKey, ControlBusKey, ExtMem}
import freechips.rocketchip.devices.debug.{DebugModuleKey, ExportDebug, JTAG}
import freechips.rocketchip.devices.tilelink.{DevNullParams, BootROMLocated}
import freechips.rocketchip.diplomacy.{DTSModel, DTSTimebase, RegionType, AddressSet}
import freechips.rocketchip.tile.{XLen}

import sifive.blocks.devices.spi.{PeripherySPIKey, SPIParams}
import sifive.blocks.devices.uart.{PeripheryUARTKey, UARTParams}

import sifive.fpgashells.shell.{DesignKey}

import testchipip.{SerialTLKey, BackingScratchpadKey}

import chipyard.{BuildSystem, ExtTLMem, DefaultClockFrequencyKey}
import java.util.prefs.BackingStoreException
import sifive.fpgashells.shell.xilinx.{EnzianDDRSize, EnableECI}

class WithDefaultPeripherals extends Config((site, here, up) => {
  case PeripheryUARTKey => List(UARTParams(address = BigInt(0x64000000L)))
})

class WithSystemModifications extends Config((site, here, up) => {
  case DTSTimebase => BigInt((1e6).toLong)
  case BootROMLocated(x) => up(BootROMLocated(x), site).map { p =>
    // invoke makefile for sdboot
    val make = s"make -C fpga/src/main/resources/enzian/bootrom bin"
    require (make.! == 0, "Failed to build bootrom")
    p.copy(hang = 0x10000, contentFileName = s"./fpga/src/main/resources/enzian/bootrom/build/bootrom.bin")
  }
  //case ExtMem => up(ExtMem, site).map(x => x.copy(master = x.master.copy(size = site(VCU118DDRSize)))) // set extmem to DDR size
  case SerialTLKey => None // remove serialized tl port
})

class WithMinSystemModifications extends Config((site, here, up) => {
  //case DTSTimebase => BigInt((0).toLong)
  case BootROMLocated(x) => up(BootROMLocated(x), site).map { p =>
    // invoke makefile for sdboot
    val make = s"make -C fpga/src/main/resources/test/sdboot bin"
    require (make.! == 0, "Failed to build bootrom")
    p.copy(hang = 0x10000, contentFileName = s"./fpga/src/main/resources/test/sdboot/build/sdboot.bin")
  }
  //case ExtMem => up(ExtMem, site).map(x => x.copy(master = x.master.copy(size = site(VCU118DDRSize)))) // set extmem to DDR size
  case SerialTLKey => None // remove serialized tl port
})

class WithECIBootROM extends Config((site, here, up) => {
  case BootROMLocated(x) => up(BootROMLocated(x), site).map { p =>
    var ram_base: BigInt = 0;
    var ram_size: BigInt = 0;
    if (site(ExtMem).nonEmpty) {
      ram_base = site(ExtMem).get.master.base
      ram_size = site(ExtMem).get.master.size
      println("Found ExtMem")
    } else if (site(ExtTLMem).nonEmpty) {
      ram_base = site(ExtTLMem).get.master.base
      ram_size = site(ExtTLMem).get.master.size
      println("Found ExtTLMem")
    } else if (site(BackingScratchpadKey).nonEmpty) {
      ram_base = site(BackingScratchpadKey).get.base
      ram_size = site(BackingScratchpadKey).get.mask + 1
      println("Found BackingScratchpad")
    } else {
      require(false, "No memory device?")
    }

    var rom_base = 0x1000000
    var rom_size = 0x400000

    // invoke makefile for sdboot
    val make = f"make -C fpga/src/main/resources/test/ecirom ROM_ORIGIN=0x${rom_base}%x ROM_LENGTH=0x${rom_size}%x RAM_ORIGIN=0x${ram_base}%x RAM_LENGTH=0x${ram_size}%x bin"
    require (make.! == 0, "Failed to build bootrom")
    p.copy(size = rom_size, address = rom_base, hang = rom_base, contentFileName = s"./fpga/src/main/resources/test/ecirom/build/ecirom.bin")
  }
})

class WithLinuxBootROM extends Config((site, here, up) => {
  case BootROMLocated(x) => up(BootROMLocated(x), site).map { p =>
    var ram_base: BigInt = 0;
    var ram_size: BigInt = 0;
    if (site(ExtMem).nonEmpty) {
      ram_base = site(ExtMem).get.master.base
      ram_size = site(ExtMem).get.master.size
      println("Found ExtMem")
    } else if (site(ExtTLMem).nonEmpty) {
      ram_base = site(ExtTLMem).get.master.base
      ram_size = site(ExtTLMem).get.master.size
      println("Found ExtTLMem")
    } else if (site(BackingScratchpadKey).nonEmpty) {
      ram_base = site(BackingScratchpadKey).get.base
      ram_size = site(BackingScratchpadKey).get.mask + 1
      println("Found BackingScratchpad")
    } else {
      require(false, "No memory device?")
    }

    var rom_base = 0x1000000
    var rom_size = 0x400000

    // invoke makefile for sdboot
    val make = f"make -C fpga/src/main/resources/test/linuxrom ROM_ORIGIN=0x${rom_base}%x ROM_LENGTH=0x${rom_size}%x RAM_ORIGIN=0x${ram_base}%x RAM_LENGTH=0x${ram_size}%x bin"
    require (make.! == 0, "Failed to build bootrom")
    p.copy(size = rom_size, address = rom_base, hang = rom_base, contentFileName = s"./fpga/src/main/resources/test/linuxrom/build/linuxrom.bin")
  }
})

class WithEnzianDDR() extends Config((site, here, up) => {
  case ExtMem => up(ExtMem, site).map(x => x.copy(master = x.master.copy(size = site(EnzianDDRSize)))) // set extmem to DDR size
  case SerialTLKey => None // remove serialized tl port
})

class WithECI() extends Config((site, here, up) => {
  case EnableECI => true
})

// DOC include start: AbstractVCU118 and Rocket
class WithEnzianTweaks extends Config(
  // harness binders
  new WithUART ++
  //new WithDDRMem ++
  // io binders
  new WithUARTIOPassthrough ++
  //new WithTLIOPassthrough ++
  // other configuration
  //new WithDefaultPeripherals ++
  //new chipyard.config.WithTLBackingMemory ++ // use TL backing memory
  new WithLinuxBootROM ++ // setup busses, use sdboot bootrom, setup ext. mem. size
  new chipyard.config.WithNoDebug ++ // remove debug module
  new freechips.rocketchip.subsystem.WithoutTLMonitors ++
  //new freechips.rocketchip.subsystem.WithNMemoryChannels(1) ++
  new WithFPGAFrequency(100) // default 100MHz freq
)

class WithEnzianTweaksDDR extends Config(
  // harness binders
  new WithUART ++
  new WithDDRMem ++
  // io binders
  new WithUARTIOPassthrough ++
  new WithTLIOPassthrough ++
  // other configuration
  new WithEnzianDDR ++
  new WithDefaultPeripherals ++
  new chipyard.config.WithTLBackingMemory ++ // use TL backing memory
  new chipyard.config.WithNoDebug ++ // remove debug module
  new freechips.rocketchip.subsystem.WithoutTLMonitors ++
  new freechips.rocketchip.subsystem.WithNMemoryChannels(1) ++
  new WithFPGAFrequency(100) // default 100MHz freq
)

class RocketNoMemEnzianConfig extends Config(
  new WithEnzianTweaks ++
  new chipyard.MbusScratchpadRocketConfig(size = BigInt(0x2000000)))

class RocketEnzianECIConfig extends Config(
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new chipyard.RocketConfig)

// Rocket Cores

// WORKING
class Rocket4xECIConfig extends Config(
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new freechips.rocketchip.subsystem.WithNBigCores(4) ++
  new chipyard.config.AbstractConfig)

// WORKING
class Rocket16xPerfConfig extends Config(
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new freechips.rocketchip.subsystem.WithNBigCores(16) ++
  new chipyard.config.WithNPerfCounters(29) ++
  new chipyard.config.AbstractConfig)

class Rocket16xL2x8PerfConfig extends Config(
  new WithFPGAFrequency(50) ++
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new freechips.rocketchip.subsystem.WithNBigCores(16) ++
  new chipyard.config.WithNPerfCounters(29) ++
  new freechips.rocketchip.subsystem.WithNBanks(8) ++
  new chipyard.config.AbstractConfig)

class Rocket8xL2x8PerfConfig extends Config(
  new WithFPGAFrequency(75) ++
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new freechips.rocketchip.subsystem.WithNBigCores(8) ++
  new chipyard.config.WithNPerfCounters(29) ++
  new freechips.rocketchip.subsystem.WithNBanks(8) ++
  new chipyard.config.AbstractConfig)

class Rocket8xL2x4PerfConfig extends Config(
  new WithFPGAFrequency(75) ++
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new freechips.rocketchip.subsystem.WithNBigCores(8) ++
  new chipyard.config.WithNPerfCounters(29) ++
  new freechips.rocketchip.subsystem.WithNBanks(4) ++
  new chipyard.config.AbstractConfig)


// Boom Cores

// WORKING
class Boom1xConfig extends Config(
  new WithFPGAFrequency(50) ++
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new chipyard.MegaBoomConfig)

// UNTESTED
class Boom1xGigaPerfConfig extends Config(
  new boom.common.WithNGigaBooms(1) ++
  new WithFPGAFrequency(50) ++
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new chipyard.config.WithNPerfCounters(29) ++
  new chipyard.config.AbstractConfig)

// WORKING
class Boom1xMegaPerfConfig extends Config(
  new boom.common.WithNMegaBooms(1) ++
  new WithFPGAFrequency(50) ++
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new chipyard.config.WithNPerfCounters(29) ++
  new chipyard.config.AbstractConfig)

// UNTESTED
class Boom1xMegaL2x2PerfConfig extends Config(
  new boom.common.WithNMegaBooms(1) ++
  new WithFPGAFrequency(50) ++
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new chipyard.config.WithNPerfCounters(29) ++
  new freechips.rocketchip.subsystem.WithNBanks(2) ++
  new chipyard.config.AbstractConfig)


// UNTESTED
class Boom2xLargePerfConfig extends Config(
  new boom.common.WithNLargeBooms(2) ++
  new WithFPGAFrequency(50) ++
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new chipyard.config.WithNPerfCounters(29) ++
  new chipyard.config.AbstractConfig)

// UNTESTED
class Boom4xMediumPerfConfig extends Config(
  new boom.common.WithNMediumBooms(4) ++
  new WithFPGAFrequency(50) ++
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new chipyard.config.WithNPerfCounters(29) ++
  new chipyard.config.AbstractConfig)

// UNTESTED
class Boom8xSmallPerfConfig extends Config(
  new boom.common.WithNSmallBooms(8) ++
  new WithFPGAFrequency(50) ++
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new chipyard.config.WithNPerfCounters(29) ++
  new chipyard.config.AbstractConfig)

// UNTESTED
class Boom8xSmallPerf75MhzConfig extends Config(
  new boom.common.WithNSmallBooms(8) ++
  new WithFPGAFrequency(75) ++
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new chipyard.config.WithNPerfCounters(29) ++
  new chipyard.config.AbstractConfig)

// Make fails
class CVA6Config extends Config(
  new WithECI ++
  new WithECIBootROM ++
  new WithEnzianTweaksDDR ++
  new chipyard.CVA6Config)

class WithFPGAFrequency(fMHz: Double) extends Config(
  new chipyard.config.WithPeripheryBusFrequency(fMHz) ++ // assumes using PBUS as default freq.
  new chipyard.config.WithMemoryBusFrequency(fMHz)
)

class WithFPGAFreq25MHz extends WithFPGAFrequency(25)
class WithFPGAFreq50MHz extends WithFPGAFrequency(50)
class WithFPGAFreq75MHz extends WithFPGAFrequency(75)
class WithFPGAFreq100MHz extends WithFPGAFrequency(100)
