![CHIPYARD](https://github.com/ucb-bar/chipyard/raw/main/docs/_static/images/chipyard-logo-full.png) | ![Enzian](https://gitlab.inf.ethz.ch/PROJECT-Enzian/fpga-sources/enzian_shell/-/raw/master/doc/img/enzian_logo.svg)

This repository contains a fork of [Chipyard](https://github.com/ucb-bar/chipyard) that works on Enzian.

# Enzian Chipyard Setup
## Repository access:

Ensure you have access to all the following repositories:

https://gitlab.inf.ethz.ch/PROJECT-Enzian/risc-v-on-enzian/fpga-shells

https://gitlab.inf.ethz.ch/PROJECT-Enzian/risc-v-on-enzian/riscv-image-builder

https://gitlab.inf.ethz.ch/PROJECT-Enzian/risc-v-on-enzian/riscv-boom

https://gitlab.inf.ethz.ch/PROJECT-Enzian/risc-v-on-enzian/rocket-chip

http://gitlab.bitcoffer.net/serverless_projects/faas_uarch_experiments.git


For `faas_uarch_experiments`, you need Michael Giardino to give you access. Talk to Michael, make an account on gitlab.bitcoffer.net, and **configure your SSH keys** (VERY IMPORTANT). Without proper ssh keys git clone WILL complain.

## Toolchain Setup
1. Run the command `sudo chmod +r /boot/vmlinuz-*`. Without this FireMarshal fails to build. See https://github.com/firesim/FireMarshal/issues/30 for more details.
2. Follow the instructions here https://chipyard.readthedocs.io/en/stable/Chipyard-Basics/Initial-Repo-Setup.html from 1.4.1.2 up until 1.4.3.
3. To recap, you must have setup conda, run `build-setup.sh` and sourced `env.sh`. **TIP** run `build-setup.sh --verbose` to see detailed errors for debugging.

## Enzian Specific Setup

Set the following environment variables:
- `export PATH=/opt/Xilinx/Vivado/2022.1/bin:$PATH`
- `export https_proxy='https://proxy.ethz.ch:3128'` and `export http_proxy='http://proxy.ethz.ch:3128'`
- `export _JAVA_OPTIONS="-Duser.home=DIR"` if Java throws an `IOException` when running sbt (NOTE THIS SOMETIMES CAUSES AN ERROR SO ONLY SET THIS IF NEEDED)

# FPGA Instructions

- Generate the bitstream in fpga/ using `make SUB_PROJECT=enzian CONFIG=YourConfig`, configs are in `fpga/src/main/scala/enzian/Configs.scala`. The resulting bitstream will be in `fpga/generated-src/chipyard.fpga.enzian.EnzianFPGATestHarness.YourConfig/obj/EnzianFPGATestHarness.bit`.
- Generate a bootable image using `software/riscv-image-builder`, instructions can be found there. It currently requires a user to be part of the `sudoer` group and installs lots of packages, so you should do this on your own machine.
- Reserve an enzian on `enzian-gateway.ethz.ch` using `emg boot zuestollXX`. Use `tmux`.
- Use `scripts/program-and-boot-enzian.py` to program the bitstream onto a reserved Enzian and boot the image, e.g.: 
  - `./load_up_enzian.py zuestollXX YourBitstream.bit --payload <payload-path> --user <nethz-username> -pw=<nethz-password>`
- On enzian-gateway, use `console zuestollXX-fpga`. USE TMUX. Login in with `enzian:enzian`. Use `stty` to resize the terminal.

# Notes
- The current MIG (Memory Interface Generator) cores only work on 16GB DIMMs (zuestoll01-03). The configuration is in `fpga/fpga-shells/src/main/scala/ip/xilinx/enzianmig`
- The `program-and-boot-enzian.py` script makes a lot of assumptions without verifying them. There's lot of room for improvement, but it mostly works. As always: Read what the script does (or tries to do) before running it.

# Old Boot Instructions
First, choose one of the Enzian machines with the 16GB RAM sticks. Then follow these steps:
1. Login into `enzian-gatweay` and run `emg boot zuestoll<number>`.
2. On another console, run `console zuestoll<number>-console`.
3. Go back to the first console and run `common_power_up()` followed by `cpu_power_up()` and `fpga_power_up()`.
4. On the second console, hit the `b` key to stop the boot process.
5. Open yet another console and run `console zuestoll<number>-fpga`.
6. Program the bitstream onto the FPGA and wait until you see "Press any key to boot" on the third console.
7. On the second console, hit the `n` key to boot normally and login once it is done booting.
8. On the second console, copy the Linux image along with `software/loadFiles/loadFiles.c` onto Enzian (e.g. using scp) and run `gcc loadFiles.c -o loadFiles`.
9. On the second console, run `sudo ./loadFiles <path_to_the_linux_image> 0`.
10. On the third console, press any key. At this point you should see "OpenSBI" and Linux should boot, finally landing you on a prompt.

# Authors
- Shashank Anand
- Diego de los Santos Gausí
- Roberto Starc
- Victor Vitez


